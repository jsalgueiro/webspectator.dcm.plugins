package com.webspectator.webservices.plugin.action;

public enum OperationInstanceStatus {
  ScaleIn,
  ScaleOut,
  Stop,
  Reannouncement,
  ReplaceInstance
}
