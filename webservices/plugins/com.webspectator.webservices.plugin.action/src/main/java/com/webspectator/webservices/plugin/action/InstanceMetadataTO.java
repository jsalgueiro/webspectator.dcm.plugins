package com.webspectator.webservices.plugin.action;

import java.io.Serializable;

public class InstanceMetadataTO implements Serializable {

  private static final long serialVersionUID = 8934284915606513739L;

  private String dns;
  private String ip;

  public InstanceMetadataTO() {

  }

  public InstanceMetadataTO(String dns, String ip) {
    this.dns = dns;
    this.ip = ip;
  }

  public String getDns() {
    return dns;
  }

  public void setDns(String dns) {
    this.dns = dns;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }
}