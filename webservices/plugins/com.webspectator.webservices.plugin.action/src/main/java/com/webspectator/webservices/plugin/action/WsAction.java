package com.webspectator.webservices.plugin.action;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.realtime.dcm.extensibility.ActionData;
import com.realtime.dcm.extensibility.ActionPlugin;
import com.realtime.dcm.extensibility.ActionPluginData;
import com.realtime.dcm.extensibility.ActivityLog;
import com.realtime.dcm.extensibility.Cluster;
import com.realtime.dcm.extensibility.Instance;
import com.realtime.dcm.extensibility.InstanceStatus;
import com.realtime.dcm.extensibility.InstanceType;
import com.realtime.dcm.extensibility.NetworkConfiguration;
import com.realtime.dcm.extensibility.Service;
import com.realtime.dcm.extensibility.httprequest.HttpRequestConfiguration;
import com.realtime.dcm.extensibility.httprequest.HttpRequestConfiguration.HttpRequestMethod;
import com.realtime.dcm.extensibility.operations.AddOperation;
import com.realtime.dcm.extensibility.operations.AddOperation.AddProviderOperation;
import com.realtime.dcm.extensibility.operations.ChangeIPOperation;
import com.realtime.dcm.extensibility.operations.ConfigureOperation;
import com.realtime.dcm.extensibility.operations.HttpRequestOperation;
import com.realtime.dcm.extensibility.operations.Operation;
import com.realtime.dcm.extensibility.operations.RemoveOperation;
import com.realtime.dcm.extensibility.operations.RemoveOperation.RemoveProviderOperation;
import com.realtime.dcm.extensibility.operations.WaitOperation;
import com.realtime.dcm.lib.Pair;
import com.realtime.dcm.lib.Strings;

public class WsAction extends ActionPlugin {

  private final Logger LOGGER = Logger.getLogger(WsAction.class);

  private ClusterMetadata clusterMetadata;

  private int totalServers;
  private ActivityLog activityLog;

  @Override
  public List<Operation> execute(ActionPluginData actionPluginData, String config) {
    activityLog = new ActivityLog(actionPluginData.getCluster());

    String clusterName = actionPluginData.getCluster().getName();
    activityLog.info("Executing action plugin for cluster " + clusterName);

    List<Operation> operations = new LinkedList<Operation>();
    Set<Instance> instances = actionPluginData.getCluster().getInstances();

    clusterMetadata = new ClusterMetadata();
    if (actionPluginData.getCluster() != null) {
      clusterMetadata.parse(actionPluginData.getCluster().getMetadata());
    }

    totalServers = (int) clusterMetadata.getRunningInstances();

    activityLog.info(clusterName + " has " + clusterMetadata.getRunningInstances() + " instances running outside the dcm scope");

    LOGGER.info("[Action Plugin ] cluster " + clusterName + " has " + clusterMetadata.getRunningInstances()
        + " instances running outside the dcm scope");

    totalServers += instances == null ? 0 : instances.size();

    LOGGER.info("[Action Plugin ] cluster " + clusterName + " has " + totalServers + " instances");

    for (ActionData action : actionPluginData.getActions()) {
      switch (action.getAction()) {
      case ScaleIn:
        operations.addAll(scaleIn(action, actionPluginData.getCluster(), config));
        break;
      case ScaleOut:
        operations.addAll(scaleOut(action, actionPluginData.getCluster(), config));
        break;
      case StartCluster:
        operations.addAll(startCluster(action, actionPluginData.getCluster(), config));
        break;
      case StopCluster:
        operations.addAll(stopCluster(action, actionPluginData.getCluster(), config));
        break;
      case Reannouncement:
        operations.addAll(reannouncement(action, actionPluginData.getCluster(), config));
        break;
      case Keep:
        operations.addAll(keep(action, actionPluginData.getCluster(), config));
        break;
      case ReplaceInstance:
        operations.addAll(replaceInstance(action, actionPluginData.getCluster(), config));
        break;
      default:
        break;
      }
    }

    return operations;
  }

  private List<Operation> scaleIn(ActionData action, Cluster cluster, String config) {
    LOGGER.info("Running SCALE IN cluster action [cluster : " + (cluster != null ? cluster.getName() : null) + " ]");

    LOGGER.info("[Scale In] cluster max instances is:  " + cluster.getMaxServers());

    List<Operation> operations = new LinkedList<Operation>();

    if (totalServers > cluster.getMinServers()) {

      int instanceOrder = 0;
      int instanceNumber;
      Instance instanceToScaleIn = null;
      for (Instance instance : cluster.getInstances()) {
        if (instance != null) {
          instanceNumber = getInstanceNumber(instance.getName());

          if (instanceNumber > instanceOrder) {
            instanceOrder = instanceNumber;
            instanceToScaleIn = instance;
          }
        }
      }
      operations.addAll(createRemoveMachineOperations(instanceToScaleIn, cluster, OperationInstanceStatus.ScaleIn));

    } else if (totalServers < cluster.getMinServers()) {
      operations.addAll(scaleOut(action, cluster, config));
    } else {
      activityLog.info("Minimum number of cluster instances has been reached for cluster " + cluster.getName());
      LOGGER.info("[Scale in] Minimum number of cluster instances has been reached");
    }
    return operations;
  }

  private List<Operation> scaleOut(ActionData action, Cluster cluster, String config) {
    LOGGER.info("Running SCALE OUT cluster action [cluster : " + (cluster != null ? cluster.getName() : null) + " ]");

    LOGGER.info("[Scale out] cluster max instances is:  " + cluster.getMaxServers());

    List<Operation> operations = new LinkedList<Operation>();

    if (totalServers < cluster.getMaxServers()) {
      // Have to take in account the instances running out of dcm scope
      int instanceOrder = totalServers + 1;
      int instanceNumber;
      for (Instance instance : cluster.getInstances()) {
        if (instance != null) {
          instanceNumber = getInstanceNumber(instance.getName());

          if (instanceNumber > instanceOrder) {
            instanceOrder = instanceNumber;
          }
        }
      }

      do {
        Service service = (Service) action.getEntity();
        if (service == null) {
          // When the scaleout is manual action doesn't have the action entity available!
          for (Instance instance : cluster.getInstances()) {
            service = new Service();
            service.setInstanceType(instance.getInstanceType());
            break;
          }
        }

        operations.addAll(createLaunchMachineOperations(cluster, service.getInstanceType(), instanceOrder));
        instanceOrder++;
      } while (totalServers < cluster.getMinServers());
    } else if (totalServers > cluster.getMaxServers()) {
      operations.addAll(scaleIn(action, cluster, config));
    } else {
      activityLog.info("Maximum number of cluster instances has been reached for cluster " + cluster.getName());
      LOGGER.info("[Scale Out] Maximum number of cluster instances has been reached");
    }

    return operations;
  }

  private List<Operation> reannouncement(ActionData action, Cluster cluster, String config) {
    List<Operation> operations = new LinkedList<Operation>();

    if (action.getActionMetadata() != null) {
      Integer instanceId = Integer.parseInt((String) action.getActionMetadata());
      Instance instance = null;

      for (Instance clusterInstance : cluster.getInstances()) {
        if (clusterInstance.getId().equals(instanceId)) {
          instance = clusterInstance;
          break;
        }
      }

      if (instance != null) {
        IpConfiguration ipConf = getInstanceIp(instance.getName());
        String instanceIp = ipConf == null ? null : ipConf.getPublicIp();

        operations.addAll(getChangeIpAndConfigurationOperations(cluster, instance.getName(), instanceIp));
      }
    }

    return operations;
  }

  private List<Operation> startCluster(ActionData action, Cluster cluster, String config) {
    LOGGER.info("Running START cluster action [cluster : " + (cluster != null ? cluster.getName() : null) + " ]");

    List<Operation> retObj = new LinkedList<Operation>();

    if (cluster != null && cluster.getAplication() != null && cluster.getAplication().getServices() != null) {

      for (Service service : cluster.getAplication().getServices()) {

        action.setEntity(service);
        LOGGER.info("[Start CLuster] trying to scale out service: " + service.getName());
        retObj.addAll(scaleOut(action, cluster, config));
      }
    } else {
      LOGGER.info("[Start Cluster] Invalid parameters");
    }

    return retObj;
  }

  private List<Operation> stopCluster(ActionData action, Cluster cluster, String config) {
    LOGGER.info("Running STOP cluster action [cluster : " + (cluster != null ? cluster.getName() : null) + " ]");
    List<Operation> operations = new LinkedList<Operation>();

    if (cluster.getInstances().size() > 0) {

      for (Instance instance : cluster.getInstances()) {
        operations.addAll(createRemoveMachineOperations(instance, cluster, OperationInstanceStatus.Stop));

        LOGGER.info("[Stop] Remove operation has been added to operation stack for cluster " + cluster.getName() + " with id "
            + cluster.getId());
      }
    }

    return operations;
  }

  private List<Operation> keep(ActionData action, Cluster cluster, String config) {
    LOGGER.info("Running KEEP cluster action [cluster : " + (cluster != null ? cluster.getName() : null) + " ]");

    List<Operation> operations = new LinkedList<Operation>();

    if (totalServers < cluster.getMinServers()) {
      activityLog.info("Performing scale out to achieve the min servers for cluster " + cluster.getName());
      operations.addAll(scaleOut(action, cluster, config));
    } else if (totalServers > cluster.getMaxServers()) {
      activityLog.info("Performing scale in to achieve the max servers for cluster " + cluster.getName());
      operations.addAll(scaleIn(action, cluster, config));
    }

    return operations;
  }

  private List<Operation> createRemoveMachineOperations(Instance instance, Cluster cluster, OperationInstanceStatus status) {
    List<Operation> operations = new LinkedList<Operation>();
    boolean isStopping = status == OperationInstanceStatus.Stop;
    RemoveOperation removeOperation;

    removeOperation = new RemoveOperation(RemoveProviderOperation.TERMINATE, !isStopping, 1 * 60 * 1000);
    removeOperation.setClusterId(cluster.getId());
    if (instance.getStatus() == InstanceStatus.Ready) {
      removeOperation.setTerminateOnRollback(true);
    }
    removeOperation.setStopCluster(isStopping);
    removeOperation.setInstanceName(instance.getName());

    String monitorRemoveInstanceURL = String.format(
        "http://monitor.webspectator.com/deleteComponentInstance?instanceId=%s&componentName=%s", instance.getInstanceId(),
        "Webservice");

    HttpRequestOperation httpReqOperation = new HttpRequestOperation(
        new HttpRequestConfiguration(monitorRemoveInstanceURL, HttpRequestMethod.POST));
    operations.add(removeOperation);
    operations.add(httpReqOperation);

    return operations;
  }

  private List<Operation> replaceInstance(ActionData action, Cluster cluster, String config) {

    LOGGER.info("[" + cluster.getName() + "] Running Replace Instance action");
    List<Operation> operations = null;
    Instance instance = action.getEntity(Instance.class);

    if (instance != null) {
      operations = new ArrayList<Operation>();

      activityLog.info("Replacing instance " + instance.getName());
      LOGGER.info("[" + cluster.getName() + "] Replacing instance " + instance.getName());

      Integer instanceNumber = getInstanceNumber(instance.getName());

      operations.addAll(createRemoveMachineOperations(instance, cluster, OperationInstanceStatus.ScaleIn));
      operations.addAll(createLaunchMachineOperations(cluster, instance.getInstanceType(), instanceNumber));

    }
    return operations;
  }

  private List<Operation> createLaunchMachineOperations(Cluster cluster, InstanceType instanceType, int instanceOrder) {
    List<Operation> operations = new LinkedList<Operation>();

    String instanceName = getInstanceName(instanceOrder);

    LOGGER.info("[Scale out] new instance name is:  " + instanceName);

    if (instanceName != null && !instanceName.trim().equals("")) {
      String newInstanceIp = null, newInstancePrivateIp = null;
      AddOperation addOperation;

      addOperation = new AddOperation(AddProviderOperation.CREATE, true, 10 * 60 * 1000);
      addOperation.setClusterId(cluster.getId());
      addOperation.setInstanceName(instanceName);
      addOperation.setInstanceTypeId(instanceType.getId());
      addOperation.setTerminateOnRollback(true);
      addOperation.setInstanceMetadata(getInstanceMetadata(instanceName));

      if (clusterMetadata.getAnnouncementFlag() != null) {
        addOperation.setRequiresAnnouncement(clusterMetadata.getAnnouncementFlag());
      } else {
        addOperation.setRequiresAnnouncement(true);
      }

      IpConfiguration instanceIp = getInstanceIp(instanceName);
      if (instanceIp != null) {

        newInstanceIp = instanceIp.getPublicIp();
        newInstancePrivateIp = instanceIp.getPrivateIp();
        LOGGER.info("[SCALE OUT] New instance IP: " + newInstanceIp);

        addOperation.setNetworkconfiguration(new NetworkConfiguration(newInstanceIp, newInstancePrivateIp,
            clusterMetadata.getSubnet(), clusterMetadata.getGateway(), clusterMetadata.getTimeoutTimer()));
      }

      operations.add(addOperation);

      LOGGER.info("[Scale Out] Add operation has been added to operation stack");
      operations.add(getConfigurationOperation(cluster, instanceName));

      totalServers++;
    }

    return operations;
  }

  private Operation getConfigurationOperation(Cluster cluster, String instanceName) {

    ConfigureOperation configureOperation = new ConfigureOperation(true, 10 * 60 * 1000);
    configureOperation.setClusterId(cluster.getId());
    configureOperation.setInstanceName(instanceName);
    configureOperation.setInstanceMetadata(getInstanceMetadata(instanceName));

    LOGGER.info("[Scale Out] Configure operation has been added to operation stack");

    return configureOperation;
  }

  private List<Operation> getChangeIpAndConfigurationOperations(Cluster cluster, String instanceName, String newInstanceIp) {
    List<Operation> operations = new LinkedList<>();
    String dns = instanceName;
    if (newInstanceIp != null) {
      ChangeIPOperation changeIpOperation = new ChangeIPOperation(true, 2 * 60 * 1000);

      changeIpOperation.setClusterId(cluster.getId());
      changeIpOperation.setDns(dns);
      changeIpOperation.setInstanceName(instanceName);
      changeIpOperation.setIpAddress(newInstanceIp);
      changeIpOperation.setManual(false);

      operations.add(changeIpOperation);

      WaitOperation waitOperation = new WaitOperation();
      waitOperation.setClusterId(cluster.getId());
      waitOperation.setWaitTime(60);

      operations.add(waitOperation);

      LOGGER.info("[Scale Out] ChangeIP operation has been added to operation stack");
    }

    operations.add(getConfigurationOperation(cluster, instanceName));
    LOGGER.info("[Scale Out] Configure operation has been added to operation stack");

    return operations;
  }

  private String getInstanceMetadata(String dns) {
    String retObj = dns;
    if (Strings.isNotBlank(dns)) {
      retObj = dns.substring(0, dns.indexOf(clusterMetadata.getDomain()) - 1);
    }
    return retObj;
  }

  private String getInstanceName(int instanceNumber) {
    String retObj = null;

    int index = instanceNumber - 1;
    Pair<String, IpConfiguration> instanceConfig = clusterMetadata.getInstances().get(index);
    if (instanceConfig != null) {
      retObj = instanceConfig.getName();
    }
    return retObj;
  }

  private int getInstanceNumber(String instanceName) {
    int retObj = -1;

    List<Pair<String, IpConfiguration>> instancesList = clusterMetadata.getInstances();
    for (int i = 0; i < instancesList.size(); i++) {
      Pair<String, IpConfiguration> instanceConf = instancesList.get(i);
      if (Strings.equals(instanceConf.getName(), instanceName)) {
        retObj = i + 1;
        break;
      }
    }
    LOGGER.debug("Instance " + instanceName + " has the index " + retObj);
    return retObj;
  }

  private IpConfiguration getInstanceIp(String instanceName) {
    IpConfiguration retObj = null;

    for (Pair<String, IpConfiguration> pair : clusterMetadata.getInstances()) {
      if (Strings.equals(instanceName, pair.getName())) {
        retObj = pair.getValue();
        break;
      }
    }
    return retObj;
  }
}