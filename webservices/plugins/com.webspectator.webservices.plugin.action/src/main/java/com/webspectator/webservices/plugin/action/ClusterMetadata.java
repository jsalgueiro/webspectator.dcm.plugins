package com.webspectator.webservices.plugin.action;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.realtime.dcm.lib.Pair;

public class ClusterMetadata {
  private String domain;
  private String subnet;
  private String gateway;
  private Boolean announcementFlag;
  private long timeoutTimer;
  private boolean hasNetworkConfig;
  private long minRunningTime;
  private List<Pair<String, IpConfiguration>> instances;
  private long runningInstances;

  public ClusterMetadata() {
    minRunningTime = 0;
    instances = new ArrayList<Pair<String, IpConfiguration>>();
  }

  public void parse(String config) {
    if (config != null) {
      JSONObject json = (JSONObject) (JSONValue.parse(config));
      this.hasNetworkConfig = false;
      this.announcementFlag = true;

      if (json.containsKey("Action")) {
        JSONObject actionJson = (JSONObject) json.get("Action");

        if (actionJson.containsKey("domain")) {
          this.domain = (String) actionJson.get("domain");
        }

        if (actionJson.containsKey("announcement")) {
          this.setAnnouncementFlag((Boolean) actionJson.get("announcement"));
        }

        if (actionJson.containsKey("runningInstances")) {
          this.setRunningInstances((Long) actionJson.get("runningInstances"));
        }

        if (actionJson.containsKey("minRunningTime")) {
          this.minRunningTime = (Long) actionJson.get("minRunningTime");
        }

        if (actionJson.containsKey("ips")) {
          JSONArray ipsJson = (JSONArray) actionJson.get("ips");
          String name = null, publicIp = null, privateIp = null;
          for (Object value : ipsJson) {
            JSONObject ipJson = (JSONObject) value;

            if (ipJson.containsKey("dns")) {
              name = (String) ipJson.get("dns");
            }

            if (ipJson.containsKey("privateIp")) {
              privateIp = (String) ipJson.get("privateIp");
            }

            if (ipJson.containsKey("publicIp")) {
              publicIp = (String) ipJson.get("publicIp");
            }

            this.instances.add(new Pair<String, IpConfiguration>(name, new IpConfiguration(publicIp, privateIp)));
          }
        }

        if (actionJson.containsKey("networkConfiguration")) {
          this.hasNetworkConfig = true;
          JSONObject networkJSON = (JSONObject) actionJson.get("networkConfiguration");
          this.subnet = ((String) networkJSON.get("subnet"));
          this.gateway = ((String) networkJSON.get("gateway"));
          if (networkJSON.get("timeout") != null) {
            this.timeoutTimer = ((Long) networkJSON.get("timeout"));
          } else {
            this.timeoutTimer = 120000;
          }
        }

      }
    }
  }

  public boolean getHasNetworkConfig() {
    return hasNetworkConfig;
  }

  public void setHasNetworkConfig(boolean hasNetworkConfig) {
    this.hasNetworkConfig = hasNetworkConfig;
  }

  public long getMinRunningTime() {
    return minRunningTime;
  }

  public void setMinRunningTime(long minRunningTime) {
    this.minRunningTime = minRunningTime;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public String getSubnet() {
    return subnet;
  }

  public void setSubnet(String subnet) {
    this.subnet = subnet;
  }

  public String getGateway() {
    return gateway;
  }

  public void setGateway(String gateway) {
    this.gateway = gateway;
  }

  public long getTimeoutTimer() {
    return timeoutTimer;
  }

  public void setTimeoutTimer(long timeoutTimer) {
    this.timeoutTimer = timeoutTimer;
  }

  public Boolean getAnnouncementFlag() {
    return announcementFlag;
  }

  public void setAnnouncementFlag(Boolean annoucementFlag) {
    this.announcementFlag = annoucementFlag;
  }

  public long getRunningInstances() {
    return runningInstances;
  }

  public void setRunningInstances(long runningInstances) {
    this.runningInstances = runningInstances;
  }

  public List<Pair<String, IpConfiguration>> getInstances() {
    return instances;
  }

}