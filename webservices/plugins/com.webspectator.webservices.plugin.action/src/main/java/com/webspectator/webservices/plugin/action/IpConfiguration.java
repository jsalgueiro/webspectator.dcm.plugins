package com.webspectator.webservices.plugin.action;

public class IpConfiguration
{
  private String publicIp;
  private String privateIp;
  
  public IpConfiguration() {}
  
  public IpConfiguration(String publicIp, String privateIp)
  {
    this.publicIp = publicIp;
    this.privateIp = privateIp;
  }
  
  public String getPublicIp()
  {
    return this.publicIp;
  }
  
  public void setPublicIp(String publicIp)
  {
    this.publicIp = publicIp;
  }
  
  public String getPrivateIp()
  {
    return this.privateIp;
  }
  
  public void setPrivateIp(String privateIp)
  {
    this.privateIp = privateIp;
  }
}