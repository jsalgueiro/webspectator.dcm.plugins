package com.webspectator.webservices.plugin.configuration;

import org.apache.log4j.Logger;

import com.realtime.dcm.extensibility.ActivityLog;
import com.realtime.dcm.extensibility.Cluster;
import com.realtime.dcm.extensibility.Configuration;
import com.realtime.dcm.extensibility.ConfigurationPlugin;
import com.realtime.dcm.extensibility.Instance;

public class WsConfiguration extends ConfigurationPlugin {

	private static final Logger LOGGER = Logger.getLogger(WsConfiguration.class);

	private ActivityLog activityLog;

	@Override
	public Configuration getConfiguration(Instance instance, Cluster cluster, String pluginMetadata) {
		LOGGER.debug("[Configuration Plugin] executing configuration plugin");

		Configuration retObj = new Configuration();
		;

		if (cluster == null || instance == null) {
			LOGGER.error("[Configuration Plugin] Provided cluster or instance are invalid!");
			return retObj;
		}

		activityLog = new ActivityLog(cluster);
		activityLog.info("Executing configuration plugin for instance " + instance.getName());

		retObj.setClusterId(cluster.getId());
		// instance metadata should be the the hostname
		retObj.setInstanceConfiguration(instance.getMetadata());
		retObj.setInstanceId(instance.getId());
		retObj.setInstanceMetadata(instance.getMetadata());

		return retObj;
	}

	@Override
	public String sendConfigurationResult(Boolean result, Configuration configuration) {

		return null;
	}
}
