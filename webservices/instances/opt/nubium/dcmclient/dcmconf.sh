#!/bin/bash -x
# set new webservice host name
sudo sed -i 's/WS_HOST_NAME/'"$1"'/' /opt/wildfly/domain/configuration/host-slave.xml

#start wildfly
sudo service wildfly start

exit 0